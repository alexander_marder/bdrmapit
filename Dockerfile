#FROM python:bullseye
FROM ubuntu:20.04
WORKDIR /app
COPY . /app
ARG COMMIT_TIME
RUN apt-get update && apt-get install -y software-properties-common && apt-get update && \
    add-apt-repository ppa:matthewluckie/scamper && apt-get update && \
    apt-get install -y gcc python3 python3-pip scamper lbzip2
RUN pip install cython traceutils2
RUN python3 setup.py sdist bdist_wheel build_ext && pip install -e .
RUN echo $COMMIT_TIME
